//
//  Cancelable.swift
//  Raft
//
//  Created by NAZARENKO Denis on 15/02/2019.
//

import Foundation

@objc public protocol Cancellable {
    func cancel()
}
