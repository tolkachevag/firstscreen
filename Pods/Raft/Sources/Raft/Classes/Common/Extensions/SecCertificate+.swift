//
//  SecCertificate+.swift
//  Raft
//
//  Created by NAZARENKO Denis on 22/02/2019.
//

import Foundation

extension SecCertificate {
    /// The public key for `self`, if it can be extracted.
    var publicKey: SecKey? {
        let policy = SecPolicyCreateBasicX509()
        var trust: SecTrust?
        let trustCreationStatus = SecTrustCreateWithCertificates(self, policy, &trust)
        
        guard let createdTrust = trust, trustCreationStatus == errSecSuccess else { return nil }
        
        return SecTrustCopyPublicKey(createdTrust)
    }
}
