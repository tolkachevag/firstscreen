//
//  NSNumber+.swift
//  Raft
//
//  Created by NAZARENKO Denis on 21/02/2019.
//

import Foundation

extension NSNumber {
    var isBool: Bool { return CFBooleanGetTypeID() == CFGetTypeID(self) }
}
