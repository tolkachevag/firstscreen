//
//  Result+.swift
//  Raft
//
//  Created by NAZARENKO Denis on 12/04/2019.
//

import Foundation

public extension Swift.Result {
    var value: Success? {
        switch self {
        case .success(let value):
            return value
        case .failure:
            return nil
        }
    }
    
    var failure: Failure? {
        switch self {
        case .failure(let error):
            return error
        case .success:
            return nil
        }
    }
}
