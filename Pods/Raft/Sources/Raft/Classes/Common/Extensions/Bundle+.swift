//
//  Bundle+.swift
//  Raft
//
//  Created by NAZARENKO Denis on 22/02/2019.
//

import Foundation

extension Bundle {
    /// Returns all valid `cer`, `crt`, and `der` certificates in the bundle.
    public var certificates: [SecCertificate] {
        return paths(forResourcesOfTypes: [".cer", ".CER", ".crt", ".CRT", ".der", ".DER"]).compactMap { path in
            guard
                let certificateData = try? Data(contentsOf: URL(fileURLWithPath: path)) as CFData,
                let certificate = SecCertificateCreateWithData(nil, certificateData)
            else { return nil }
            
            return certificate
        }
    }
    
    /// Returns all public keys for the valid certificates in the bundle.
    public var publicKeys: [SecKey] {
        return certificates.publicKeys
    }
    
    /// Returns all pathnames for the resources identified by the provided file extensions.
    ///
    /// - Parameter types: The filename extensions locate.
    /// - Returns:         All pathnames for the given filename extensions.
    func paths(forResourcesOfTypes types: [String]) -> [String] {
        return Array(Set(types.flatMap { paths(forResourcesOfType: $0, inDirectory: nil) }))
    }
    
    public func certificate(with name: String, type: String) -> SecCertificate? {
        guard let url = Bundle.main.url(forResource: name, withExtension: type),
              let localCertificate = try? Data(contentsOf: url) as CFData
        else { return nil }

        return SecCertificateCreateWithData(nil, localCertificate)
    }
}
