//
//  SecTrust+.swift
//  Raft
//
//  Created by NAZARENKO Denis on 22/02/2019.
//

import Foundation
extension SecTrust {
    func setAnchorCertificates(_ certificates: [SecCertificate]) throws {
        // Add additional anchor certificates.
        let status = SecTrustSetAnchorCertificates(self, certificates as CFArray)
        guard status.isSuccess else {
            throw Raft.Error.serverTrustEvaluationFailed
        }
        
        // Reenable system anchor certificates.
        let systemStatus = SecTrustSetAnchorCertificatesOnly(self, true)
        guard systemStatus.isSuccess else {
            throw Raft.Error.serverTrustEvaluationFailed
        }
    }
    
    var certificateData: [Data] {
        return certificates.data
    }
    
    var certificates: [SecCertificate] {
        return (0..<SecTrustGetCertificateCount(self)).compactMap { index in
            SecTrustGetCertificateAtIndex(self, index)
        }
    }
}
