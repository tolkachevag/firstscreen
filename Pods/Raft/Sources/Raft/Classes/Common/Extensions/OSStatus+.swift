//
//  OSStatus+.swift
//  Raft
//
//  Created by NAZARENKO Denis on 22/02/2019.
//

import Foundation

extension OSStatus {
    var isSuccess: Bool { return self == errSecSuccess }
}
