//
//  NSLock+.swift
//  Raft
//
//  Created by NAZARENKO Denis on 27/02/2019.
//

import Foundation

extension NSLock {
    func withCriticalScope<T>(_ block: () -> T) -> T {
        lock()
        defer { unlock() }
        return block()
    }
}
