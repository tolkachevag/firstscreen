//
//  Array+.swift
//  Raft
//
//  Created by NAZARENKO Denis on 01/03/2019.
//

import Foundation

extension Array where Element == SecCertificate {
    /// All `Data` values for the contained `SecCertificate`s.
    var data: [Data] {
        return map { SecCertificateCopyData($0) as Data }
    }
    
    /// All public `SecKey` values for the contained `SecCertificate`s.
    public var publicKeys: [SecKey] {
        return compactMap { $0.publicKey }
    }
}
