//
//  URLRequest+.swift
//  Raft
//
//  Created by NAZARENKO Denis on 21/02/2019.
//

import Foundation

extension URLRequest {
    mutating func encoded(encodable: Encodable, encoder: JSONEncoder = JSONEncoder()) throws -> URLRequest {
        do {
            let encodable = AnyEncodable(encodable)
            httpBody = try encoder.encode(encodable)
            
            let contentTypeHeaderName = "Content-Type"
            if value(forHTTPHeaderField: contentTypeHeaderName) == nil {
                setValue("application/json", forHTTPHeaderField: contentTypeHeaderName)
            }
            
            return self
        } catch {
            throw Raft.Error.parameterEncodingFailed(error)
        }
    }
    
    func encoded(parameters: [String: Any], parameterEncoding: ParameterEncoding) throws -> URLRequest {
        do {
            return try parameterEncoding.encode(self, with: parameters)
        } catch {
            throw Raft.Error.parameterEncodingFailed(error)
        }
    }

    mutating func update(with body: Data, parameters: [String: Any]) -> URLRequest {
        httpBody = body
        guard let url = self.url,
              var components = URLComponents(url: url, resolvingAgainstBaseURL: false) else {
            return self
        }
        components.queryItems = parameters.compactMap { key, value in
            guard let stringValue = value as? String else {
                return nil
            }
            return URLQueryItem(name: key, value: stringValue)
        }
        self.url = components.url
        return self
    }

    public var curlString: String {
        guard let url = url else { return "\n🤦🏼‍♂️ пустой url?!\n" }
        
        guard let method = httpMethod else { return "\n🌚 отсутствует httpMethod. How?\n" }
        
        var baseCommand = String(format: "curl -k -X %@", method)
        
        if let headers = allHTTPHeaderFields {
            for (key, value) in headers {
                let escapedValue = value.replacingOccurrences(of: "\"", with: "\\\"")
                let escapedKey = key.replacingOccurrences(of: "\"", with: "\\\"")
                let row = String(format: " \\\n -H \"%@: %@\"", escapedKey, escapedValue)
                baseCommand.append(row)
            }
        }
        
        if let data = httpBody, let body = String(data: data, encoding: .utf8) {
            let row = String.init(format: " \\\n -d '%@'", body.replacingOccurrences(of: "'", with: "\\'"))
            baseCommand.append(row)
        }
        
        return baseCommand.appendingFormat(" \\\n \"%@\" --dump-header -", url.absoluteString)
    }
}
