//
//  URLRequest+Multipart.swift
//  Raft
//
//  Created by KOROTKOV Nikolay on 19.11.2020.
//

import Foundation

private enum RequestEncodingError: String, Swift.Error {
    case invalidData
}

extension URLRequest {
    mutating func encoded(formData: [MultipartFormData], parameters: [String: String]?) throws -> URLRequest {

        let boundary = UUID().uuidString

        self.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        var data = Data()
        try formData.forEach {
            try data.addMultiPart(boundary: boundary,
                                  name: $0.name,
                                  filename: $0.fileName,
                                  contentType: $0.mimeType,
                                  data: $0.data)
        }

        if let parameters = parameters {
            try data.addParameters(boundary: boundary, parameters: parameters)
        }

        try data.closeMultiPart(boundary: boundary)

        self.httpBody = data
        return self
    }
}

private extension Data {
    mutating func addMultiPart(boundary: String, name: String, filename: String, contentType: String, data: Data) throws {

        guard let upperBoundaryData = "\r\n--\(boundary)\r\n".data(using: .utf8),
              let contentDispositionData = "Content-Disposition: form-data; name=\"\(name)\"; filename=\"\(filename)\"\r\n".data(using: .utf8),
              let contentTypeData = "Content-Type: \(contentType)\r\n\r\n".data(using: .utf8),
              let lineBreak = "\r\n".data(using: .utf8)
        else {
            throw RequestEncodingError.invalidData
        }

        self.append(upperBoundaryData)
        self.append(contentDispositionData)
        self.append(contentTypeData)
        self.append(data)
        self.append(lineBreak)
    }

    mutating func addParameters(boundary: String, parameters: [String: String]) throws {
        let lineBreak = "\r\n"
        for (key, value) in parameters {
            guard let upperBoundaryData = "--\(boundary + lineBreak)".data(using: .utf8),
                  let contentDispositionData = "Content-Disposition: form-data; name=\"\(key)\"\(lineBreak + lineBreak)".data(using: .utf8),
                  let contentValueData = "\(value + lineBreak)".data(using: .utf8)
            else {
                throw RequestEncodingError.invalidData
            }

            self.append(upperBoundaryData)
            self.append(contentDispositionData)
            self.append(contentValueData)
        }
    }

    mutating func closeMultiPart(boundary: String) throws {
        guard let closingBoundaryData = "--\(boundary)--\r\n".data(using: .utf8)
        else {
            throw RequestEncodingError.invalidData
        }
        self.append(closingBoundaryData)
    }
}
