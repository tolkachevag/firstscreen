//
//  QueueProvider.swift
//  Raft
//
//  Created by NAZARENKO Denis on 09/04/2019.
//

import Foundation

public protocol QueueProvider: class {
    func queue(for marker: Marker) -> OperationQueue?
    func register(queue: OperationQueue, for marker: Marker)
    @discardableResult
    func unregisterQueue(for marker: Marker) -> OperationQueue?
}
