//
//  UniqueIdentifiable.swift
//  Raft
//
//  Created by NAZARENKO Denis on 19/02/2019.
//

import Foundation

public protocol UniqueIdentifiable {
    var id: String { get }
}

public extension UniqueIdentifiable {
    var id: String { return String(describing: self) }
}
