//
//  Identifiable.swift
//  ObjectMapper
//
//  Created by OMELCHUK Daniil on 25.09.2020.
//

import Foundation

public protocol Identifiable {
  /// A type representing the stable identity of the entity associated with self.
  associatedtype ID: Hashable

  /// The stable identity of the entity associated with self.
  var id: Self.ID { get }
}
