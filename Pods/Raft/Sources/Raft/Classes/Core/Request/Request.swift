//
//  Request.swift
//  Raft
//
//  Created by NAZARENKO Denis on 12/02/2019.
//

import Foundation

public typealias Path = String

public final class Request {
    public let identifier = UUID()
    public let marker: Marker
    public var baseURL: URL?
    public var path: Path?
    public var method: HTTPMethod = .get
    public var type: Option = .plain
    public var headers = HTTPHeaders()
    public var sessionConfiguration = SessionConfiguration()
    public var trust: TrustValidator = .default
    public var cookieNames: [String] = []
    
    public init(marker: Marker) {
        self.marker = marker
    }
}

extension Request: Equatable {
    public static func == (lhs: Request, rhs: Request) -> Bool {
        return lhs.identifier == rhs.identifier
    }
}

extension Request: Identifiable {
    public var id: String { return identifier.uuidString }
}
