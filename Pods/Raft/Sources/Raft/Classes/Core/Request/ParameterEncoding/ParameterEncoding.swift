//
//  ParameterEncoding.swift
//  Raft
//
//  Created by NAZARENKO Denis on 20/02/2019.
//

import Foundation

public typealias Parameters = [String: Any]

public protocol ParameterEncoding {
    func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest
}
