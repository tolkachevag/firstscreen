//
//  RequestTask.swift
//  Raft
//
//  Created by NAZARENKO Denis on 18/02/2019.
//

import Foundation

extension Request {
/// Represents an HTTP task.
    public enum Option {

    /// A request with no additional data.
    case plain

    /// A requests body set with data.
    case data(Data)

    /// A request body set with `Encodable` type
    case jsonEncodable(Encodable)

    /// A request body set with `Encodable` type and custom encoder
    case customJSONEncodable(Encodable, encoder: JSONEncoder)

    /// A requests body set with encoded parameters.
    case parameters(parameters: [String: Any], encoding: ParameterEncoding)

    /// A requests body set with data, combined with url parameters.
    case compositeData(bodyData: Data, urlParameters: [String: Any])

    /// A requests body set with encoded parameters combined with url parameters.
    case compositeParameters(bodyParameters: [String: Any], bodyEncoding: ParameterEncoding, urlParameters: [String: Any])

    /// A file upload task.
    case uploadFile(URL)

    /// A "multipart/form-data" upload task combined with url parameters.
    case uploadMultipart([MultipartFormData], urlParameters: [String: String]? = nil)

    /// A file download task to a destination.
    case downloadDestination(URL)
//
//    /// A file download task to a destination with extra parameters using the given encoding.
//    case downloadParameters(parameters: [String: Any], encoding: ParameterEncoding, destination: DownloadDestination)
    }
}
