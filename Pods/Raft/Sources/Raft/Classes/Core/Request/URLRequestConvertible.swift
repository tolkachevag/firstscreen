//
//  URLRequestConvertible.swift
//  Raft
//
//  Created by NAZARENKO Denis on 22/02/2019.
//

import Foundation

public protocol URLRequestConvertible {
    func toURLRequest() throws -> URLRequest
}

extension URLRequest: URLRequestConvertible {
    public func toURLRequest() throws -> URLRequest {
        return self
    }
}
