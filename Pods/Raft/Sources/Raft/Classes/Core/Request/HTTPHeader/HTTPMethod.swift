//
//  HTTPMethod.swift
//  Raft
//
//  Created by NAZARENKO Denis on 13/02/2019.
//

import Foundation

public enum HTTPMethod: RawRepresentable {
    
    public typealias RawValue = String
    
    case connect
    case delete
    case get
    case head
    case options
    case patch
    case post
    case put
    case trace
    case custom(_ method: String)
    
    public var rawValue: String {
        switch  self {
        case .connect: return "CONNECT"
        case .delete: return "DELETE"
        case .get: return "GET"
        case .head: return "HEAD"
        case .options: return "OPTIONS"
        case .patch: return "PATCH"
        case .post: return "POST"
        case .put: return "PUT"
        case .trace: return "TRACE"
        case .custom(let method): return method.uppercased()
        }
    }
    
    public static let standardMethods: [HTTPMethod] = [.connect, .delete, .get,
                                                       .head, .options, .patch,
                                                       .post, .put, .trace]
    
    public init?(rawValue: String) {
        let rawValue = rawValue.uppercased()
        
        for type in HTTPMethod.standardMethods where type.rawValue == rawValue {
            self = type
            
            return
        }
        
        self = .custom(rawValue)
    }
}
