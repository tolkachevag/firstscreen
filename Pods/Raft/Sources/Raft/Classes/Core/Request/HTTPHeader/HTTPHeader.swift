//
//  HTTPHeader.swift
//  Raft
//
//  Created by NAZARENKO Denis on 14/02/2019.
//

import Foundation

public struct HTTPHeader: Hashable {
    /// Name of the header.
    public let name: String
    
    /// Value of the header.
    public let value: String
    
    /// Creates an instance from the given `name` and `value`.
    ///
    /// - Parameters:
    ///   - name:  The name of the header.
    ///   - value: The value of the header.
    public init(name: String, value: String) {
        self.name = name
        self.value = value
    }
}

extension HTTPHeader: CustomStringConvertible {
    public var description: String {
        return "\(name): \(value)"
    }
}

extension HTTPHeader {
    /// Returns an `Accept-Charset` header.
    ///
    /// - Parameter value: The `Accept-Charset` value.
    /// - Returns:         The header.
    public static func acceptCharset(_ value: String) -> HTTPHeader {
        return HTTPHeader(name: "Accept-Charset", value: value)
    }
    
    /// Returns an `Accept-Language` header.
    ///
    /// - Parameter value: The `Accept-Language` value.
    /// - Returns:         The header.
    public static func acceptLanguage(_ value: String) -> HTTPHeader {
        return HTTPHeader(name: "Accept-Language", value: value)
    }
    
    /// Returns an `Accept-Encoding` header.
    ///
    /// - Parameter value: The `Accept-Encoding` value.
    /// - Returns:         The header
    public static func acceptEncoding(_ value: String) -> HTTPHeader {
        return HTTPHeader(name: "Accept-Encoding", value: value)
    }
    
    /// Returns a `Basic` `Authorization` header using the `username` and `password` provided.
    ///
    /// - Parameters:
    ///   - username: The username of the header.
    ///   - password: The password of the header.
    /// - Returns:    The header.
    public static func authorization(username: String, password: String) -> HTTPHeader {
        let credential = Data("\(username):\(password)".utf8).base64EncodedString()
        
        return authorization("Basic \(credential)")
    }
    
    /// Returns a `Bearer` `Authorization` header using the `bearerToken` provided
    ///
    /// - Parameter bearerToken: The bearer token.
    /// - Returns:               The header.
    public static func authorization(bearerToken: String) -> HTTPHeader {
        return authorization("Bearer \(bearerToken)")
    }
    
    /// Returns an `Authorization` header.
    ///
    /// - Parameter value: The `Authorization` value.
    /// - Returns:         The header.
    public static func authorization(_ value: String) -> HTTPHeader {
        return HTTPHeader(name: "Authorization", value: value)
    }

    /// Returns Id token header.
    ///
    /// - Parameter token: The JWT or other type token.
    /// - Returns:         The header.
    public static func idToken(_ token: String) -> HTTPHeader {
      return HTTPHeader(name: "Id-Token", value: token)
    }
    
    /// Returns a `Content-Disposition` header.
    ///
    /// - Parameter value: The `Content-Disposition` value.
    /// - Returns:         The header.
    public static func contentDisposition(_ value: String) -> HTTPHeader {
        return HTTPHeader(name: "Content-Disposition", value: value)
    }
    
    /// Returns a `Content-Type` header.
    ///
    /// - Parameter value: The `Content-Type` value.
    /// - Returns:         The header.
    public static func contentType(_ value: String) -> HTTPHeader {
        return HTTPHeader(name: "Content-Type", value: value)
    }
    
    public static var contentTypeJSON: HTTPHeader {
        return contentType("application/json")
    }
    
    /// Returns a `User-Agent` header.
    ///
    /// - Parameter value: The `User-Agent` value.
    /// - Returns:         The header.
    public static func userAgent(_ value: String) -> HTTPHeader {
        return HTTPHeader(name: "User-Agent", value: value)
    }
    
    public static func rcDevice(_ value: String) -> HTTPHeader {
        return HTTPHeader(name: "RC-Device", value: value)
    }
    
    public static func accept(_ value: String) -> HTTPHeader {
        return HTTPHeader(name: "Accept", value: value)
    }
}
