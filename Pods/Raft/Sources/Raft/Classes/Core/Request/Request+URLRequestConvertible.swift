//
//  Request+URLRequestConvertible.swift
//  Raft
//
//  Created by NAZARENKO Denis on 28/02/2019.
//

import Foundation

extension Request: URLRequestConvertible {
    public func toURLRequest() throws -> URLRequest {
        // URL
        guard let baseUrl = baseURL else { throw Raft.Error.missingUrl }

        // Path

        let specificUrl: URL

        if let path = path, !path.isEmpty {
            specificUrl = baseUrl.appendingPathComponent(path)
        } else {
            specificUrl = baseUrl
        }

        guard let processedURL = URLProcessor.process(specificUrl) else { throw Raft.Error.missingUrl }

        var urlRequest = URLRequest(url: processedURL)
        urlRequest.httpMethod = method.rawValue

        // Headers
        urlRequest.allHTTPHeaderFields = headers.dictionary

        // Parameters
        switch type {
        case .plain, .uploadFile, .downloadDestination:
            return urlRequest
        case .data(let data):
            urlRequest.httpBody = data
            return urlRequest
        case .jsonEncodable(let encodable):
            return try urlRequest.encoded(encodable: encodable)
        case let .parameters(parameters, encoding):
            return try urlRequest.encoded(parameters: parameters, parameterEncoding: encoding)
        case let .uploadMultipart(formData, urlParameters):
            return try urlRequest.encoded(formData: formData, parameters: urlParameters)
        case let .compositeData(bodyData, urlParameters):
            return urlRequest.update(with: bodyData, parameters: urlParameters)
        default:
            return urlRequest
        }
    }
}
