//
//  SessionConfiguration.swift
//  Raft
//
//  Created by NAZARENKO Denis on 28/02/2019.
//

import Foundation

public final class SessionConfiguration {
    /// If you want to use custom Background Session Configuration, please set identifier
    /// If it nil then used URLSessionConfiguration.ephemeral or .default depends on available
    public var sessionIdentifier: String?

    /// Default value for EPHEMERAL and DEFAULT session configuration
    public var shouldUseExtendedBackgroundIdleMode: Bool = false

    /// Default value for EPHEMERAL and DEFAULT session configuration
    public var sessionSendsLaunchEvents: Bool = false

    /// Default value for EPHEMERAL and DEFAULT session configuration
    public var isDiscretionary: Bool = false

    /// Default value for EPHEMERAL and DEFAULT session configuration
    public var allowsCellularAccess: Bool = true

    /// Request timeout interval for all tasks
    public var timeoutIntervalForRequest: TimeInterval = 60

    /// Resource timeout interval for all tasks
    public var timeoutIntervalForResource: TimeInterval = 60

    /// Cookie storage object used by all tasks
    public var httpShouldSetCookies: Bool = true

    /// Ignore local cache data, and instruct proxies and other intermediates to disregard their caches so far as the protocol allows.
    public var cachePolicy: URLRequest.CachePolicy = .reloadIgnoringLocalAndRemoteCacheData

    /// TODO: Please set description !!!
    public var protocolClasses: [AnyClass]?

    func create() -> URLSessionConfiguration {
        let session: URLSessionConfiguration
        if let sessionIdentifier = sessionIdentifier {
          session = URLSessionConfiguration.background(withIdentifier: sessionIdentifier)
        } else {
          session = URLSessionConfiguration.ephemeral.copy(with: nil) as? URLSessionConfiguration ?? .default
        }

        session.timeoutIntervalForRequest = timeoutIntervalForRequest
        session.timeoutIntervalForResource = timeoutIntervalForResource
        session.requestCachePolicy = cachePolicy
        session.httpShouldSetCookies = httpShouldSetCookies

        session.shouldUseExtendedBackgroundIdleMode = shouldUseExtendedBackgroundIdleMode
        #if os(iOS)
            session.sessionSendsLaunchEvents = sessionSendsLaunchEvents
        #endif
        session.isDiscretionary = isDiscretionary
        session.allowsCellularAccess = allowsCellularAccess

        if let protocolClasses = protocolClasses {
            session.protocolClasses = protocolClasses
        }
        
        return session
    }
}
