//
//  MultipartFormData.swift
//  Raft
//
//  Created by KOROTKOV Nikolay on 19.11.2020.
//

import Foundation

public struct MultipartFormData {
    public let data: Data
    public let name: String
    public let fileName: String
    public let mimeType: String
    
    public init(data: Data, mimeType: String, name: String, fileName: String) {
        self.data = data
        self.name = name
        self.fileName = fileName
        self.mimeType = mimeType
    }
}
