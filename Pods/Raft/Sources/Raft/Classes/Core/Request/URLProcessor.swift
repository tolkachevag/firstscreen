//
//  URLProcessor.swift
//  Raft
//
//  Created by KOROTKOV Nikolay on 22.04.2020.
//

import Foundation

private let pattern = "([^:])(\\/{2,})"

private extension String {
    var fullRange: NSRange {
        return NSRange(location: 0, length: self.utf16.count)
    }
}

/// Removes unnecessary multiple forward slashes, but only if not preceded by ":"
final class URLProcessor {

    private static let regex: NSRegularExpression = {
        guard let result = try? NSRegularExpression(pattern: pattern, options: []) else {
             fatalError("Invalid regular expression in URLTextProcessor")
        }
        return result
    }()

    static func process(_ input: URL) -> URL? {
        let string = input.absoluteString
        let result = regex.stringByReplacingMatches(in: string, options: [], range: string.fullRange, withTemplate: "$1/")
        
        return URL(string: result)
    }
    
}
