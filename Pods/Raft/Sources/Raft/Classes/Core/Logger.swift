//
//  Logger.swift
//  Raft
//
//  Created by GOLOFAEV Roman on 05.08.2020.
//

import Foundation
import os.log

public class Logger {
    public enum Level {
        case info
        case debug
        case error
    }
    
    public static var enabledLevel = Set<Level>()
    
    @available(iOS 10.0, *)
    static let domain = OSLog(subsystem: "ru.raiffeisen.raft", category: "Service")
    
    static func print(message: String, level: Level = .info) {
        guard enabledLevel.contains(level) else { return }
        if #available(iOS 10.0, *) {
            let message = message.replacingOccurrences(of: "%", with: "%%")
            os_log("%@", log: domain, type: .default, message)
        } else {
            Swift.print(message)
        }
    }
}
