//
//  TaskEventMonitor.swift
//  Raft
//
//  Created by NAZARENKO Denis on 20/02/2019.
//

import Foundation

public protocol RequestDelegate: class {
    
    /// Calls a delegate does he want to continue request
    /// It reduce returning value from some RequestDelegate's
    /// If someone return false the result will be `false`
    func request(_ request: Request, shouldContinueFrom state: RequestState) -> Bool
    func request(_ request: Request, didChangeStateTo state: RequestState)
}

extension RequestDelegate {
    public func request(_ request: Request, shouldContinueFrom state: RequestState) -> Bool {
        return true
    }
    
    func request(_ request: Request, didChangeStateTo state: RequestState) { }
}
