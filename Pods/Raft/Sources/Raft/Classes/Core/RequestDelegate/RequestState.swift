//
//  RequestState.swift
//  Raft
//
//  Created by NAZARENKO Denis on 04/03/2019.
//

import Foundation

public enum RequestState {
    case created
    case progress(Progress)
    case cancelled
    case finished(Response)
}

extension RequestState: CustomStringConvertible {
    public var description: String {
        switch self {
        case .created : return "created"
        case .progress(let value): return "progress: (\(value))"
        case .cancelled: return "cancelled"
        case .finished(let response): return "finished. response: \(response)"
        }
    }
}
