//
//  RequestProxyDelegate.swift
//  Raft
//
//  Created by NAZARENKO Denis on 22/02/2019.
//

import Foundation

public final class RequestDelegateProxy: RequestDelegate {
    
    private var multicast = RequestDelegateMulticast()
    
    public func request(_ request: Request, didChangeStateTo state: RequestState) {
        multicast.invoke { $0.request(request, didChangeStateTo: state) }
    }
    
    public func request(_ request: Request, shouldContinueFrom state: RequestState) -> Bool {
        return multicast.reduce(block: { $0.request(request, shouldContinueFrom: state) }, initial: true)
    }
    
    func invoke(_ block: (RequestDelegate) -> Void) {
        multicast.invoke(block: block)
    }
    
    func reduce(_ block: Transform<RequestDelegate, Bool>, initial: Bool = false) -> Bool {
        return multicast.reduce(block: block, initial: true)
    }
    
    func add(delegate: RequestDelegate) {
        multicast.add(delegate: delegate)
    }
    
    func remove(delegate: RequestDelegate) {
        multicast.remove(delegate: delegate)
    }
    
    func contain(delegate: RequestDelegate) -> Bool {
        return multicast.contain(delegate: delegate)
    }
}
