//
//  MulticastDelegate.swift
//  Raft
//
//  Created by NAZARENKO Denis on 20/02/2019.
//

import Foundation

// MARK: RequestDelegateMulticast
struct RequestDelegateMulticast {
    private var delegates: [RequestDelegateWeakBox] = []
    
    mutating func add(delegate: RequestDelegate) {
        delegates.append(RequestDelegateWeakBox(value: delegate))
    }

    mutating func remove(delegate: RequestDelegate) {
        delegates = delegates.filter { $0.value !== delegate }
    }
    
    func contain(delegate: RequestDelegate) -> Bool {
        return delegates.compactMap { $0.value }.contains(where: { $0 === delegate })
    }

    func invoke(block: Block<RequestDelegate>) {
        delegates.forEach { box in
            if let monitor = box.value {
                block(monitor)
            }
        }
    }
    
    func reduce(block: Transform<RequestDelegate, Bool>, initial: Bool = false) -> Bool {
        return delegates.reduce(initial, { (result, next) -> Bool in
            if let monitor = next.value {
                return result && block(monitor)
            } else {
                return result
            }
        })
    }
}
