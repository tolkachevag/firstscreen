//
//  WeakBox.swift
//  Raft
//
//  Created by NAZARENKO Denis on 20/02/2019.
//

import Foundation

struct RequestDelegateWeakBox {
    weak var value: RequestDelegate?
}
