//
//  BaseURLProvider.swift
//  Raft
//
//  Created by GOLOFAEV Roman on 05.08.2020.
//

import Foundation

public protocol BaseURLProvider {
    var baseURL: URL? { get set }
}
