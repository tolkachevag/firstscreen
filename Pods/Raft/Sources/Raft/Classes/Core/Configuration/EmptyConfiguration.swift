//
//  EmptyConfiguration.swift
//  Raft
//
//  Created by NAZARENKO Denis on 07/03/2019.
//

import Foundation

/// Use this class if you need empty configuration
public final class EmptyConfiguration: Configurable {
    
    public init() { }
    
    public func apply(to request: inout Request) { }
}
