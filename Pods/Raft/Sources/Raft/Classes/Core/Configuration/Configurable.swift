//
//  Configurable.swift
//  Raft
//
//  Created by NAZARENKO Denis on 19/02/2019.
//

import Foundation

// MARK: Configurable
public protocol Configurable: UniqueIdentifiable {
    func apply(to request: inout Request)
}
