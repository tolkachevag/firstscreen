//
//  WorkerDelegate.swift
//  Raft
//
//  Created by NAZARENKO Denis on 14/02/2019.
//

import Foundation

// MARK: WorkerDelegate
public protocol WorkerDelegate: class {
    func worker(_ worker: Worker, didLoadResponse response: HTTPURLResponse, data: Data?)
    func worker(_ worker: Worker, didFailWithError error: Swift.Error)
    func worker(_ worker: Worker, didChange progress: Progress)
}

public extension WorkerDelegate {
    func worker(_ worker: Worker, didLoadResponse response: HTTPURLResponse, data: Data?) { }
    func worker(_ worker: Worker, didChange progress: Progress) { }
    func worker(_ worker: Worker, didFailWithError error: Swift.Error) { }
}
