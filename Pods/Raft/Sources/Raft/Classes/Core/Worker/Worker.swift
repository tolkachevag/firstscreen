//
//  Worker.swift
//  Raft
//
//  Created by NAZARENKO Denis on 13/02/2019.
//

import Foundation

public typealias TrustEvaluationResult = (URLSession.AuthChallengeDisposition, URLCredential?)
public typealias TrustResolveBlock = (URLAuthenticationChallenge) throws -> TrustEvaluationResult
public typealias CancellableWorker = Worker & Cancellable

// MARK: Worker 
public protocol Worker: class {
    func perform(request: URLRequest)
    var delegate: WorkerDelegate? { get set }
    var sessionConfiguration: URLSessionConfiguration { get set }
    var trustEvaluation: TrustResolveBlock? { get set }
}
