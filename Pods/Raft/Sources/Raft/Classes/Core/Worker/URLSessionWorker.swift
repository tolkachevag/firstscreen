//
//  URLSessionWorker.swift
//  Raft
//
//  Created by NAZARENKO Denis on 14/02/2019.
//

import Foundation

// MARK: - URLSessionWorker
public final class URLSessionWorker: NSObject, CancellableWorker {
    
    // MARK: - Public properties
    public var trustEvaluation: TrustResolveBlock?
    public weak var delegate: WorkerDelegate?
    public var sessionConfiguration: URLSessionConfiguration = .ephemeral
    public lazy var session: URLSession = {
        let session = URLSession(configuration: sessionConfiguration,
                                 delegate: self,
                                 delegateQueue: nil)
        return session
    }()
    
    // MARK: Private properties
    private var buffer = NSMutableData()
    private var response: URLResponse?
    private var expectedContentLength: Int64 = 0
    private var task: URLSessionDataTask?
    
    // MARK: Initializerss
    public override init() { }
    
    // MARK: Public Methods
    
    public func perform(request: URLRequest) {
        task = session.dataTask(with: request)
        task?.resume()
    }
    
    public func cancel() {
        session.invalidateAndCancel()
    }
    
    deinit {
        session.invalidateAndCancel()
    }
}

// MARK: URLSessionDelegate
extension URLSessionWorker: URLSessionDelegate {
    
    public func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Swift.Error?) {
        defer { session.invalidateAndCancel() }
        
        if let error = error {
            if let error = error as NSError?,
                error.code == CFNetworkErrors.cfurlErrorCancelled.rawValue {
                return
            }
            
            delegate?.worker(self, didFailWithError: error)
        } else if let response = task.response as? HTTPURLResponse {
            delegate?.worker(self, didLoadResponse: response, data: buffer as Data)
        }
    }
    
    public func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        buffer.append(data)
        let percentage = Double(buffer.length) / Double(expectedContentLength)
        let progress = Progress(percentage: .download(percentage), contentSize: expectedContentLength, rate: 0)
        delegate?.worker(self, didChange: progress)
    }
}

// MARK: URLSessionDataDelegate
extension URLSessionWorker: URLSessionDataDelegate {
    public func urlSession(_ session: URLSession,
                           dataTask: URLSessionDataTask,
                           didReceive response: URLResponse,
                           completionHandler: @escaping (URLSession.ResponseDisposition) -> Void) {
        self.response = response
        expectedContentLength = response.expectedContentLength
        completionHandler(.allow)
    }
}

// MARK: URLSessionTaskDelegate
extension URLSessionWorker: URLSessionTaskDelegate {
    public func urlSession(_ session: URLSession,
                           task: URLSessionTask,
                           didSendBodyData bytesSent: Int64,
                           totalBytesSent: Int64,
                           totalBytesExpectedToSend: Int64) {
        let percentage = Double(totalBytesSent) / Double(totalBytesExpectedToSend)
        let progress = Progress(percentage: .upload(percentage), contentSize: totalBytesExpectedToSend, rate: 0)
        delegate?.worker(self, didChange: progress)
    }
    
    public func urlSession(_ session: URLSession,
                           didReceive challenge: URLAuthenticationChallenge,
                           completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {

        guard let trustEvaluation = trustEvaluation else {
            completionHandler(.performDefaultHandling, nil)
            
            return
        }
        
        do {
            let trustResult = try trustEvaluation(challenge)
            completionHandler(trustResult.0, trustResult.1)
        } catch {
            completionHandler(.cancelAuthenticationChallenge,nil)
        }
    }
}
