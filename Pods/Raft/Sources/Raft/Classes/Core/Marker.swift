//
//  Marker.swift
//  Raft
//
//  Created by NAZARENKO Denis on 20/02/2019.
//

import Foundation

public class Marker: CustomStringConvertible, Identifiable {
    public var id: String { return identifier }

    // MARK: - Public properties
    public var description: String {
        return identifier
    }
    
    // MARK: - Private properties
    private let identifier: String
    
    // MARK: - Initializers
    public init(_ identifier: String) {
        self.identifier = identifier
    }
  
    public required convenience init?(rawValue: String) {
        self.init(rawValue)
    }
    
    public required convenience init(stringLiteral value: String) {
        self.init(value)
    }
}

// MARK: Hashable
extension Marker: Hashable {
    public static func == (lhs: Marker, rhs: Marker) -> Bool {
        return lhs.identifier == rhs.identifier
    }
    
    public func hash(into hasher: inout Hasher) {
        hasher.combine(identifier)
    }
}

extension Marker: RawRepresentable {
    public var rawValue: String { return identifier }
}

extension Marker: ExpressibleByStringLiteral {}
