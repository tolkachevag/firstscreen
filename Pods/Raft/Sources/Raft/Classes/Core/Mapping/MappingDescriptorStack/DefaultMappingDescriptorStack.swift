//
//  DefaultMappingDescriptorStack.swift
//  Raft
//
//  Created by NAZARENKO Denis on 15/05/2019.
//

import Foundation

// MARK: DefaultMappingDescriptorStack
public final class DefaultMappingDescriptorStack<Value, Error: Swift.Error>: MappingDescriptorStack where Error: RaftErrorInitializable {
    
    // TODO: удалить codeValueDescriptors -- удалить, можно использовать rangeValueDescriptors
    var codeValueDescriptors: [StatusCode: AnyMappingDescriptor<Value>] = [:]
    var codeErrorDescriptors: [StatusCode: AnyMappingDescriptor<Error>] = [:]
     
    var rangeValueDescriptors: [AnyRangeMappingDescriptor<Value>] = []
    var rangeErrorDescriptors: [AnyRangeMappingDescriptor<Error>] = []
    
    public init() { }
    
    public func addValueDescriptor<T: MappingDescriptor>(_ valueDescriptor: T, for statusCode: StatusCode) where T.Value == Value {
        guard codeValueDescriptors[statusCode] == nil
            else { fatalError("🤦🏼‍♂️ Value Descriptor already appended for \(statusCode) code.") }
        
        codeValueDescriptors[statusCode] = AnyMappingDescriptor<Value>(valueDescriptor)
    }
    
    public func addErrorDescriptor<U: MappingDescriptor>(_ errorDescriptor: U, for statusCode: StatusCode) where U.Value == Error {
        guard codeErrorDescriptors[statusCode] == nil
            else { fatalError("🤦🏼‍♂️ Error Descriptor already appended for \(statusCode) code.") }
        
        codeErrorDescriptors[statusCode] = AnyMappingDescriptor<Error>(errorDescriptor)
    }
    
    public func addValueDescriptor<T: MappingDescriptor>(_ valueDescriptor: T,
                                                         for statusCodes: ClosedRange<StatusCode>) where T.Value == Value {
        guard !rangeValueDescriptors.reduce(false, { $0 || $1.range.overlaps(statusCodes) })
            else { fatalError("🤦🏼‍♂️ Value Descriptor already appended for range \(statusCodes) or its subrange.") }
        
        rangeValueDescriptors.append(.init(descriptor: valueDescriptor, range: statusCodes))
    }
    
    public func addErrorDescriptor<U: MappingDescriptor>(_ errorDescriptor: U,
                                                         for statusCodes: ClosedRange<StatusCode>) where U.Value == Error {
        guard !rangeErrorDescriptors.reduce(false, { $0 || $1.range.overlaps(statusCodes) })
            else { fatalError("🤦🏼‍♂️ Error Descriptor already appended for range \(statusCodes) or its subrange.") }
        
        rangeErrorDescriptors.append(.init(descriptor: errorDescriptor, range: statusCodes))
    }

    public func removeValueDescriptor<T: MappingDescriptor>(_ valueDescriptor: T,
                                                            for statusCode: StatusCode) where T.Value == Value {
        guard codeValueDescriptors.contains(where: { $0.0 == statusCode })
            else { fatalError("🤦🏼‍♂️ Value Descriptor is not appended for \(statusCode) code.") }

        codeValueDescriptors.removeValue(forKey: statusCode)
    }

    public func removeValueDescriptor<T: MappingDescriptor>(_ valueDescriptor: T,
                                                         for statusCodes: ClosedRange<StatusCode>) where T.Value == Value {
        guard rangeValueDescriptors.reduce(false, { $0 || $1.range.overlaps(statusCodes) })
            else { fatalError("🤦🏼‍♂️ Value Descriptor already removed for range \(statusCodes) or its subrange.") }

        rangeValueDescriptors.removeAll { $0.range == statusCodes }
    }

    public func valueDescriptor(for statusCode: StatusCode) -> AnyMappingDescriptor<Value>? {
        return codeValueDescriptors[statusCode] ?? rangeValueDescriptors
                                                    .first(where: { $0.range.contains(statusCode) })?
                                                    .anyMappingDesriptor
    }
    
    public func errorDescriptor(for statusCode: StatusCode) -> AnyMappingDescriptor<Error>? {
        return codeErrorDescriptors[statusCode] ?? rangeErrorDescriptors
                                                    .first(where: { $0.range.contains(statusCode) })?
                                                    .anyMappingDesriptor
    }
}
