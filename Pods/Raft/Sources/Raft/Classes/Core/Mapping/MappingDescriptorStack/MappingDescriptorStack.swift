//
//  MappingDescriptorStack.swift
//  Raft
//
//  Created by NAZARENKO Denis on 14/05/2019.
//

import Foundation

public protocol MappingDescriptorStack {
    associatedtype Value
    associatedtype Error: Swift.Error where Error: RaftErrorInitializable
    
    func addValueDescriptor<T: MappingDescriptor>(_ valueDescriptor: T,
                                                  for statusCode: StatusCode) where T.Value == Value
    func addErrorDescriptor<U: MappingDescriptor>(_ errorDescriptor: U,
                                                  for statusCode: StatusCode) where U.Value == Error
    func addValueDescriptor<T: MappingDescriptor>(_ valueDescriptor: T,
                                                  for statusCodes: ClosedRange<StatusCode>) where T.Value == Value
    func addErrorDescriptor<U: MappingDescriptor>(_ errorDescriptor: U,
                                                  for statusCodes: ClosedRange<StatusCode>) where U.Value == Error
    func removeValueDescriptor<U: MappingDescriptor>(_ valueDescriptor: U,
                                                     for statusCode: StatusCode) where U.Value == Value
    func removeValueDescriptor<U: MappingDescriptor>(_ valueDescriptor: U,
                                                     for statusCodes: ClosedRange<StatusCode>) where U.Value == Value
    
    func valueDescriptor(for statusCode: StatusCode) -> AnyMappingDescriptor<Value>?
    func errorDescriptor(for statusCode: StatusCode) -> AnyMappingDescriptor<Error>?
}
