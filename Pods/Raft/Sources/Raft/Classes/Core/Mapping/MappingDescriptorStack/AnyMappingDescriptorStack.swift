//
//  AnyMappingDescriptorStack.swift
//  Raft
//
//  Created by NAZARENKO Denis on 14/05/2019.
//

import Foundation

// MARK: Type Erasure for MappingDescriptorStack protocol

// MARK: AnyMappingDescriptorStackBase - Base class mocking a protocol with associated values
internal class AnyMappingDescriptorStackBase<Value, Error: Swift.Error>: MappingDescriptorStack where Error: RaftErrorInitializable {
    func addValueDescriptor<T>(_ valueDescriptor: T, for statusCode: StatusCode) where T : MappingDescriptor, Value == T.Value {
        fatalError()
    }
    
    func addErrorDescriptor<U>(_ errorDescriptor: U, for statusCode: StatusCode) where U : MappingDescriptor, Error == U.Value {
        fatalError()
    }
    
    func addValueDescriptor<T>(_ valueDescriptor: T, for statusCodes: ClosedRange<StatusCode>) where T : MappingDescriptor, Value == T.Value {
        fatalError()
    }
    
    func addErrorDescriptor<U>(_ errorDescriptor: U, for statusCodes: ClosedRange<StatusCode>) where U : MappingDescriptor, Error == U.Value {
        fatalError()
    }

    func removeValueDescriptor<T>(_ valueDescriptor: T, for statusCode: StatusCode) where T : MappingDescriptor, Value == T.Value {
        fatalError()
    }
    
    func removeValueDescriptor<T>(_ valueDescriptor: T, for statusCodes: ClosedRange<StatusCode>) where T : MappingDescriptor, Value == T.Value {
        fatalError()
    }
    
    func valueDescriptor(for statusCode: StatusCode) -> AnyMappingDescriptor<Value>? {
        fatalError()
    }
    
    func errorDescriptor(for statusCode: StatusCode) -> AnyMappingDescriptor<Error>? {
        fatalError()
    }
}

// MARK: AnyMappingDescriptorStackBox - Box, containing any implementation of MappingDescriptorStack protocol
internal class AnyMappingDescriptorStackBox<Base: MappingDescriptorStack>: AnyMappingDescriptorStackBase<Base.Value, Base.Error> {
    let base: Base
    
    init(base: Base) {
        self.base = base
    }
    
    override func addValueDescriptor<T>(_ valueDescriptor: T, for statusCode: StatusCode) where Value == T.Value, T : MappingDescriptor {
        base.addValueDescriptor(valueDescriptor, for: statusCode)
    }
    
    override func addErrorDescriptor<U>(_ errorDescriptor: U, for statusCode: StatusCode) where Error == U.Value, U : MappingDescriptor {
        base.addErrorDescriptor(errorDescriptor, for: statusCode)
    }
    
    override func addValueDescriptor<T>(_ valueDescriptor: T, for statusCodes: ClosedRange<StatusCode>) where Value == T.Value, T : MappingDescriptor {
        base.addValueDescriptor(valueDescriptor, for: statusCodes)
    }
    
    override func addErrorDescriptor<U>(_ errorDescriptor: U, for statusCodes: ClosedRange<StatusCode>) where Error == U.Value, U : MappingDescriptor {
        base.addErrorDescriptor(errorDescriptor, for: statusCodes)
    }
    
    override func valueDescriptor(for statusCode: StatusCode) -> AnyMappingDescriptor<Base.Value>? {
        return base.valueDescriptor(for: statusCode)
    }
    
    override func errorDescriptor(for statusCode: StatusCode) -> AnyMappingDescriptor<Base.Error>? {
        return base.errorDescriptor(for: statusCode)
    }

    override func removeValueDescriptor<T>(_ valueDescriptor: T, for statusCode: StatusCode) where Value == T.Value, T : MappingDescriptor {
        base.removeValueDescriptor(valueDescriptor, for: statusCode)
    }

    override func removeValueDescriptor<T>(_ valueDescriptor: T, for statusCodes: ClosedRange<StatusCode>) where Value == T.Value, T : MappingDescriptor {
        base.removeValueDescriptor(valueDescriptor, for: statusCodes)
    }
}

// MARK: AnyMappingDescriptorStack
public struct AnyMappingDescriptorStack<Value, Error: Swift.Error>: MappingDescriptorStack where Error: RaftErrorInitializable {
    
    let box: AnyMappingDescriptorStackBase<Value, Error>
    
    init<Base: MappingDescriptorStack>(_ base: Base) where Base.Value == Value, Base.Error == Error {
        box = AnyMappingDescriptorStackBox(base: base)
    }

    public func addValueDescriptor<U: MappingDescriptor>(_ valueDescriptor: U, for statusCode: StatusCode) where U.Value == Value {
        box.addValueDescriptor(valueDescriptor, for: statusCode)
    }
    
    public func addErrorDescriptor<U: MappingDescriptor>(_ errorDescriptor: U, for statusCode: StatusCode) where U.Value == Error {
        box.addErrorDescriptor(errorDescriptor, for: statusCode)
    }
    
    public func addValueDescriptor<U: MappingDescriptor>(_ valueDescriptor: U,
                                                         for statusCodes: ClosedRange<StatusCode>) where U.Value == Value {
        box.addValueDescriptor(valueDescriptor, for: statusCodes)
    }
    
    public func addErrorDescriptor<U: MappingDescriptor>(_ errorDescriptor: U,
                                                         for statusCodes: ClosedRange<StatusCode>) where U.Value == Error {
        box.addErrorDescriptor(errorDescriptor, for: statusCodes)
    }
    
    public func valueDescriptor(for statusCode: StatusCode) -> AnyMappingDescriptor<Value>? {
        return box.valueDescriptor(for: statusCode)
    }
 
    public func errorDescriptor(for statusCode: StatusCode) -> AnyMappingDescriptor<Error>? {
        return box.errorDescriptor(for: statusCode)
    }

    public func removeValueDescriptor<U: MappingDescriptor>(_ valueDescriptor: U, for statusCode: StatusCode) where U.Value == Value {
        box.removeValueDescriptor(valueDescriptor, for: statusCode)
    }

    public func removeValueDescriptor<U: MappingDescriptor>(_ valueDescriptor: U,
                                                         for statusCodes: ClosedRange<StatusCode>) where U.Value == Value {
        box.removeValueDescriptor(valueDescriptor, for: statusCodes)
    }
}
