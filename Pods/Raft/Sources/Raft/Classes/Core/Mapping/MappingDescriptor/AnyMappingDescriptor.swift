//
//  AnyMappingDescriptor.swift
//  Raft
//
//  Created by NAZARENKO Denis on 17/02/2019.
//

import Foundation

public typealias InOutBlock<T> = (inout T) -> Void
public typealias PostmappingBlock<T> = (Data?, inout T) -> Void

// MARK: - AnyMappingDescriptor
public struct AnyMappingDescriptor<Value>: MappingDescriptor {

    // MARK: - Public properties
    
    public let premapping: Transform<Data?,Data?>?
    public let postmapping: PostmappingBlock<Value>?
    public let mapping: Transform<Data?,Value?>
    
    // MARK: Internal properties
    let base: Any
    
    // MARK: - Initializers
    
    public init<Descriptor: MappingDescriptor>(_ base: Descriptor) where Descriptor.Value == Value {
        self.base        = base
        self.premapping  = base.premapping
        self.mapping     = base.map
        self.postmapping = base.postmapping
    }
    
    // MARK: Public methods
    
    public func map(from data: Data?) -> Value? {
        return mapping(data)
    }
}
