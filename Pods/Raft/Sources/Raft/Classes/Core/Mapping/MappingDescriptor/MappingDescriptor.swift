//
//  MappingDescriptor.swift
//  Raft
//
//  Created by NAZARENKO Denis on 17/02/2019.
//

import Foundation

public typealias Transform<Input, Output> = (Input) -> Output
public typealias PairTransform<FirstInput, SecondInput, Output> = (FirstInput, SecondInput) -> Output

public protocol MappingDescriptor {
    associatedtype Value
    
    /// it prepares data before mapping
    var premapping: Transform<Data?, Data?>? { get }
    /// it's just a mapping: Data -> Object
    func map(from data: Data?) -> Value?
    /// it allows to make something with mapped object before return it to smth
    var postmapping: PostmappingBlock<Value>? { get }
    
    /// perform mapping of object using `premapping`, `map(from:)`, `postmapping`
    ///
    /// - Parameter data: from response
    /// - Returns: just an object
    func perform(using data: Data?) -> Value?
}

extension MappingDescriptor {
    public func perform(using data: Data?) -> Value? {
        var premappedData = data
        
        if let premapping = premapping {
            premappedData = premapping(data)
        }
        
        let object = map(from: premappedData)
        
        if postmapping == nil {
            return object
        }
        
        if let postmap = postmapping, var object = object {
            postmap(premappedData, &object)
            return object
        }
        
        return object
    }
}
