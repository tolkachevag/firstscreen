//
//  ValueMappingDescriptor.swift
//  ObjectMapper
//
//  Created by ZAVYALOV Yaroslav on 21.04.2020.
//

import Foundation

// MARK: - JSONMappingDescriptor
public final class JSONMappingDescriptor<Value>: MappingDescriptor {
    /// Preparing data for mapping
    public let premapping: Transform<Data?, Data?>?

    /// Mapping object
    public let postmapping: PostmappingBlock<Value>?

    /// JSON decoder
    public var decoder = JSONDecoder()

    /// Init class with `premapping`, `postmapping`, `postmapping`
    ///
    /// - Parameters:
    ///       premapping: Preparing data for mapping
    ///       postmapping: Mapping object
    ///       postmapping: JSON decoder
    public init(premapping: Transform<Data?, Data?>? = nil, postmapping: PostmappingBlock<Value>? = nil) {
        self.premapping = premapping
        self.postmapping = postmapping
    }
}

// MARK: - Publics
extension JSONMappingDescriptor {
    /// Map Data to object using optional `data`
    ///
    /// - Parameter data: JSON as a data from responce
    /// - Returns: object
    public func map(from data: Data?) -> Value? {
        if Value.self == Void.self, (data == nil || data?.count == 0) {
            return () as? Value
        }
        
        do {
            return try decode(model: Value.self, data: data)
        } catch {
            Logger.print(
                message: "mapping error: \(error)",
                level: .error
            )
          return nil
        }
    }
}

// MARK: - Privates
extension JSONMappingDescriptor {
    private func decode<T>(model: T.Type, data: Data?) throws -> T? {
        guard let data = data else { return nil }
        guard let decodableType = model as? Decodable.Type else { return nil }
        do {
            let result = try decodableType.init(jsonData: data)
            return result as? T
        } catch let error {
            Logger.print(message: (error as NSError).debugDescription, level: .error)
            return nil
        }
    }
}

private extension Decodable {
    init(jsonData: Data) throws {
        self = try JSONDecoder().decode(Self.self, from: jsonData)
    }
}
