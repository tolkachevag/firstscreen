//
//  AnyRangeMappingDescriptor.swift
//  Raft
//
//  Created by NAZARENKO Denis on 15/05/2019.
//

import Foundation

public struct AnyRangeMappingDescriptor<Value> {
    let anyMappingDesriptor: AnyMappingDescriptor<Value>
    let range: ClosedRange<StatusCode>
    
    init<Descriptor: MappingDescriptor>(descriptor: Descriptor,
                                        range: ClosedRange<StatusCode>) where Descriptor.Value == Value {
        self.anyMappingDesriptor = AnyMappingDescriptor<Value>(descriptor)
        self.range = range
    }
}
