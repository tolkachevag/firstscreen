//
//  AnyMappingResolver.swift
//  Raft
//
//  Created by NAZARENKO Denis on 17/02/2019.
//

import Foundation

// MARK: - AnyMappingStrategy
public final class AnyMappingStrategy<Value, Error: Swift.Error>: MappingStrategy where Error: RaftErrorInitializable {
    
    // MARK: - Private properties
    
    let base: Any
    let map: (Response, AnyMappingDescriptorStack<Value, Error>) -> Result<Value, Error>

    // MARK: - Initializers
    
    public init<U: MappingStrategy>(_ base: U) where U.Value == Value, U.Error == Error {
        self.base = base
        self.map  = base.perform
    }
    
    // MARK: - Public methods
    
    public func perform<U>(response: Response,
                           stack: U) -> Result<Value, Error> where U : MappingDescriptorStack, Error == U.Error, Value == U.Value {
        return map(response, AnyMappingDescriptorStack<Value, Error>(stack))
    }
}
