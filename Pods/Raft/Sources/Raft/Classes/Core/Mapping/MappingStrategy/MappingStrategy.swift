//
//  MappingResolver.Swift
//  Raft
//
//  Created by NAZARENKO Denis on 17/02/2019.
//

import Foundation

// MARK: - MappingStrategy
public protocol MappingStrategy: class {
    associatedtype Value
    associatedtype Error: Swift.Error where Error: RaftErrorInitializable
    
    func perform<U: MappingDescriptorStack>(response: Response, stack: U) -> Result<Value, Error> where U.Error == Error, U.Value == Value
}

public extension MappingStrategy {
    func perform<U: MappingDescriptorStack>(response: Response, stack: U) -> Result<Value, Error> where U.Error == Error, U.Value == Value {
        let error = response.error
        let data = response.data
        
        if let error = error {
            let nsError = error as NSError
            let urlError = URLError(
                URLError.Code(rawValue: nsError.code),
                userInfo: [NSUnderlyingErrorKey: error]
            )
            let raftError =  Raft.Error.network(urlError)
                
            return .failure(Error(raftError))
        } else {
            
            let valueDescriptorOrNil: AnyMappingDescriptor<Value>? = stack.valueDescriptor(for: response.statusCode)
            
            if let valueDescriptor = valueDescriptorOrNil,
                let mappedValue = valueDescriptor.perform(using: data) {
                return .success(mappedValue)
            }
            
            let errorDescriptorOrNil: AnyMappingDescriptor<Error>? = stack.errorDescriptor(for: response.statusCode)
            if let errorDescriptor = errorDescriptorOrNil,
                let mappedError = errorDescriptor.perform(using: data) {
                    return .failure(mappedError)
            } else {
                let raftError = Raft.Error.mappingError(response)
            
                return .failure(Error(raftError))
            }
        }
    }
}
