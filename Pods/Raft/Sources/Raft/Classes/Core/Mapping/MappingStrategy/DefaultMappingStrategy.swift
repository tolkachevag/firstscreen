//
//  DefaultMappingStrategy.swift
//  Raft
//
//  Created by NAZARENKO Denis on 15/05/2019.
//

import Foundation

// MARK: DefaultMappingStrategy
public final class DefaultMappingStrategy<Value, Error: Swift.Error>: MappingStrategy where Error: RaftErrorInitializable {
    public init() { }
}
