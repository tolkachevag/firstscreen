//
//  RaftMapper.swift
//  Raft
//
//  Created by NAZARENKO Denis on 12/02/2019.
//

import Foundation

// MARK: Mapper
public final class Mapper<Value, Error: Swift.Error> where Error: RaftErrorInitializable {
    
    // MARK: - Private properties
    
    var strategy = AnyMappingStrategy<Value, Error>(DefaultMappingStrategy<Value, Error>())
    var descriptorStack = AnyMappingDescriptorStack<Value, Error>(DefaultMappingDescriptorStack<Value, Error>())
    var defaultJSONDescriptor: JSONMappingDescriptor? = JSONMappingDescriptor<Value>()

    public init() {
        if let defaultJSONDescriptor = self.defaultJSONDescriptor {
            descriptorStack.addValueDescriptor(defaultJSONDescriptor, for: 200...299)
        }
    }
    
    // MARK: - Public methods

    public func addValueDescriptor<T: MappingDescriptor>(_ valueDescriptor: T, for statusCode: StatusCode) where T.Value == Value {
        descriptorStack.addValueDescriptor(valueDescriptor, for: statusCode)
    }
    
    public func addErrorDescriptor<U: MappingDescriptor>(_ errorDescriptor: U, for statusCode: StatusCode) where U.Value == Error {
        descriptorStack.addErrorDescriptor(errorDescriptor, for: statusCode)
    }
    
    public func addValueDescriptor<T: MappingDescriptor>(_ valueDescriptor: T,
                                                         for statusCodes: ClosedRange<StatusCode>) where T.Value == Value {
        if let defaultJSONDescriptor = self.defaultJSONDescriptor {
            descriptorStack.removeValueDescriptor(defaultJSONDescriptor, for: 200...299)
            self.defaultJSONDescriptor = nil
        }

        descriptorStack.addValueDescriptor(valueDescriptor, for: statusCodes)
    }
    
    public func addErrorDescriptor<U: MappingDescriptor>(_ errorDescriptor: U,
                                                         for statusCodes: ClosedRange<StatusCode>) where U.Value == Error {
        descriptorStack.addErrorDescriptor(errorDescriptor, for: statusCodes)
    }
    
    public func valueDescriptor(for statusCode: StatusCode) -> AnyMappingDescriptor<Value>? {
        return descriptorStack.valueDescriptor(for: statusCode)
    }
    
    public func errorDescriptor(for statusCode: StatusCode) -> AnyMappingDescriptor<Error>? {
        return descriptorStack.errorDescriptor(for: statusCode)
    }
    
    public func set<Strategy: MappingStrategy>(strategy: Strategy) where Strategy.Error == Error, Strategy.Value == Value {
        self.strategy = AnyMappingStrategy<Value, Error>(strategy)
    }
    
    public func set<DescriptorStack: MappingDescriptorStack>(stack: DescriptorStack) where DescriptorStack.Error == Error, DescriptorStack.Value == Value {
        self.descriptorStack = AnyMappingDescriptorStack<Value, Error>(stack)
    }
    
    func map(response: Response) -> Result<Value, Error> {
        return strategy.perform(response: response, stack: descriptorStack)
    }

}
