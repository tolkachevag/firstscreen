//
//  Progress.swift
//  Raft
//
//  Created by NAZARENKO Denis on 22/05/2019.
//

import Foundation

public struct Progress {
    public let percentage: Percentage
    public let contentSize: Int64
    public let rate: Double?
}

extension Progress: CustomStringConvertible {
    public var description: String {
        var components: [String] = []
        components.append("Progress")
        components.append(percentage.description)
        components.append("contentSize: \(contentSize)")
        let rateComponent = rate.flatMap { "rate: \($0)" }
        components.append(rateComponent ?? "rate: nothing")
        
        return components.joined(separator: " | ")
    }
}

public extension Progress {
    enum Percentage {
        case upload(Double)
        case download(Double)
    }
}

extension Progress.Percentage: CustomStringConvertible {
    public var description: String {
        switch self {
        case .download(let value):
            return "downloaded: \(value)"
        case .upload(let value):
            return "uploaded: \(value)"
        }
    }
}
