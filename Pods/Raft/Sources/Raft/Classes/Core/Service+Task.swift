//
//  Service+Task.swift
//  Raft
//
//  Created by GOLOFAEV Roman on 05.08.2020.
//

import Foundation

extension Service {
    public static func task<V, E>(_ service: Service?, marker: Marker) throws -> Task<V, E> {
        guard let service = service else {
            throw ServiceError.missingService
        }
        
        guard let task: Task<V, E> = service.task(marker: marker) else {
            throw ServiceError.missingMarker
        }
        
        return task
    }
}
