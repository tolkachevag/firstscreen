//
//  ServiceError.swift
//  Raft
//
//  Created by GOLOFAEV Roman on 05.08.2020.
//

import Foundation

enum ServiceError: Swift.Error {
    case missingService
    case missingMarker
}
