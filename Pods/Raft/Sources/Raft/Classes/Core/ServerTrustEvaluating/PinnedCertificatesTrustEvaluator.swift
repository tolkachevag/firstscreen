//
//  PinnedCertificatesTrustEvaluator.swift
//  Raft
//
//  Created by NAZARENKO Denis on 22/02/2019.
//

import Foundation

// MARK: - PinnedCertificatesTrustEvaluator
public final class PinnedCertificatesTrustEvaluator: ServerTrustEvaluating {
    
    // MARK: - Private properties
    
    private let certificates: [SecCertificate]
    private let acceptSelfSignedCertificates: Bool
    
    // MARK: - Initializers
    
    public init(certificates: [SecCertificate] = Bundle.main.certificates,
                acceptSelfSignedCertificates: Bool = false) {
        self.certificates = certificates
        self.acceptSelfSignedCertificates = acceptSelfSignedCertificates
    }
    
    // MARK: - Public methods
    
    public func evaluate(_ trust: SecTrust, for host: String) throws {
        guard !certificates.isEmpty else { throw Raft.Error.serverTrustEvaluationFailed }
        
        if acceptSelfSignedCertificates {
            try trust.setAnchorCertificates(certificates)
        }
        
        let serverCertificatesData = Set(trust.certificateData)
        let pinnedCertificatesData = Set(certificates.data)
        let pinnedCertificatesInServerData = !serverCertificatesData.isDisjoint(with: pinnedCertificatesData)
        if !pinnedCertificatesInServerData {
            throw Raft.Error.serverTrustEvaluationFailed
        }
    }
}
