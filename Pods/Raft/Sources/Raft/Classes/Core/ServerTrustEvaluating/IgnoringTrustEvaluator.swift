//
//  IgnoringTrustEvaluator.swift
//  Raft
//
//  Created by NAZARENKO Denis on 04/03/2019.
//

import Foundation

// MARK: - IgnoringTrustEvaluator
public final class IgnoringTrustEvaluator: ServerTrustEvaluating {
    public func evaluate(_ trust: SecTrust, for host: String) throws { }
    
    public init() { }
}
