//
//  ServerTrustManager.swift
//  Raft
//
//  Created by NAZARENKO Denis on 22/02/2019.
//

import Foundation

public typealias Host = String

public struct TrustValidator {
    
    private var evaluators: [String: ServerTrustEvaluating]
    
    private let shouldIgnoreAll: Bool
    
    public init(shouldIgnoreAll: Bool = false,
                evaluators: [String: ServerTrustEvaluating] = [:]) {
        self.shouldIgnoreAll = shouldIgnoreAll
        self.evaluators = evaluators
    }
    
    private func serverTrustEvaluator(for host: String) -> ServerTrustEvaluating? {
        guard !shouldIgnoreAll else { return IgnoringTrustEvaluator() }
    
        return evaluators[host]
    }
    
    public mutating func add(evaluator: ServerTrustEvaluating, for host: Host) {
        evaluators[host] = evaluator
    }
    
    @discardableResult
    public mutating func remove(evaluator: ServerTrustEvaluating, for host: Host) -> ServerTrustEvaluating? {
        return evaluators.removeValue(forKey: host)
    }
    
    func evaluateTrusting(with challenge: URLAuthenticationChallenge) throws -> TrustEvaluationResult {
    
        let evaluation: TrustEvaluationResult
        
        switch challenge.protectionSpace.authenticationMethod {
        case NSURLAuthenticationMethodServerTrust:

            let evaluationResult = attemptServerTrustAuthentication(with: challenge)
            evaluation = evaluationResult.result
            
            if evaluationResult.error != nil {
                throw Raft.Error.serverTrustEvaluationFailed
            }
        default:
             evaluation = (.performDefaultHandling, nil)
        }
        
        return evaluation
    }
    
    func attemptServerTrustAuthentication(with challenge: URLAuthenticationChallenge) -> (result: TrustEvaluationResult, error: Error?) {
        let host = challenge.protectionSpace.host
        
        guard challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust,
            let trust = challenge.protectionSpace.serverTrust
            else {
                return ((.performDefaultHandling, nil), nil)
        }
        
        do {
            guard let evaluator = self.serverTrustEvaluator(for: host) else {
                return ((.performDefaultHandling, nil), nil)
            }
            
            try evaluator.evaluate(trust, for: host)
            
            return ((.useCredential, URLCredential(trust: trust)), nil)
        } catch {
            return ((.cancelAuthenticationChallenge, nil), nil)
        }
    }
    
}

extension TrustValidator {
    static let `default` = TrustValidator()
}
