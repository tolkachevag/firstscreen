//
//  ServerTrustEvaluating.swift
//  Raft
//
//  Created by NAZARENKO Denis on 22/02/2019.
//

import Foundation

public protocol ServerTrustEvaluating {
    func evaluate(_ trust: SecTrust, for host: String) throws
}
