//
//  Task.swift
//  Raft
//
//  Created by NAZARENKO Denis on 12/02/2019.
//

import Foundation

public typealias Block<T> = (T) -> Void
public typealias PairBlock<T, U> = (T, U) -> Void
public typealias ResponseHandler = Block<Response>

/// Class implementing configurable abstraction of HTTP requests
public final class Task<V,E: Swift.Error>: Operation, Cancellable where E: RaftErrorInitializable {
    
    // MARK: - Public properties
    
    /// Producing by Raft. Configurable.
    public let request: Request
    /// Performs mapping. Configurable.
    public let mapper: Mapper<V,E>
    /// Inject this closure if you want to get just a "raw" response model.
    /// It always calls before completion handler
    public var responseHandler: ResponseHandler?
    public var completionMappingHandler: ((Response, Result<V, E>) -> Result<V, E>)?

    // MARK: - Private properties
    
    private var requestDelegateProxy: RequestDelegateProxy
    private let workerFactory: WorkerFactory
    private let operationQueue: OperationQueue
    private var completion: Block<Result<V, E>>?
    private var onProgress: Block<Progress>?
    private let cookieStorage: HTTPCookieStorage
    private lazy var worker: Worker = workerFactory.make()
    private let stateLock = NSLock()
    
    // MARK: - Initializers
    
    init(request: Request,
         workerFactory: WorkerFactory,
         executingQueue: OperationQueue,
         requestDelegateProxy: RequestDelegateProxy,
         cookieStorage: HTTPCookieStorage = .shared) {
        self.request = request
        self.workerFactory = workerFactory
        self.operationQueue = executingQueue
        self.requestDelegateProxy = requestDelegateProxy
        self.cookieStorage = cookieStorage
        self.mapper = Mapper()
    }
    
    // MARK: - Public methods
    
    public func onProgress(_ onProgress: Block<Progress>? = nil) {
        self.onProgress = onProgress
    }
        
    @discardableResult
    public func perform(onProgress: Block<Progress>? = nil,
                        completion: @escaping Block<Result<V,E>>) -> Self {
        self.completion = completion
        self.onProgress = onProgress
        operationQueue.addOperation(self)
        
        return self
    }
    
    public func add(delegate: RequestDelegate) {
        requestDelegateProxy.add(delegate: delegate)
    }
    
    public func remove(delegate: RequestDelegate) {
        requestDelegateProxy.remove(delegate: delegate)
    }
    
    // MARK: - Life Cycle
    
    override public var isAsynchronous: Bool { return true }
    
    private var _executing: Bool = false
    override private(set) public var isExecuting: Bool {
        get {
            return stateLock.withCriticalScope { _executing }
        }
        set {
            willChangeValue(forKey: "isExecuting")
            stateLock.withCriticalScope { _executing = newValue }
            didChangeValue(forKey: "isExecuting")
        }
    }
    
    private var _finished: Bool = false
    override private(set) public var isFinished: Bool {
        get {
            return stateLock.withCriticalScope { _finished }
        }
        set {
            willChangeValue(forKey: "isFinished")
            stateLock.withCriticalScope { _finished = newValue }
            didChangeValue(forKey: "isFinished")
        }
    }
        
    override public func start() {
        guard !isCancelled else {
            isFinished = true
            
            return
        }
        
        isExecuting = true
        main()
    }
    
    override public func cancel() {
        super.cancel()
        
        if let worker = worker as? CancellableWorker {
            worker.cancel()
        }
    }
    
    override public func main() {
        guard !isCancelled else {
            finish()
            
            return
        }
        
        do {
            let urlRequest = try request.toURLRequest()
            requestDelegateProxy.request(request, didChangeStateTo: .created)
            worker.delegate = self
            worker.trustEvaluation = { [request] challenge in
                return try request.trust.evaluateTrusting(with: challenge)
            }
            
            worker.sessionConfiguration = request.sessionConfiguration.create()

            Logger.print(
                message: "\n\nRAFT Request: " + request.identifier.uuidString + "\n" + urlRequest.curlString,
                level: .debug
            )

            worker.perform(request: urlRequest)
        } catch let error {
            var result: Result<V, E>
            
            switch error {
            case let raftError as Raft.Error:
                result = .failure(E.init(raftError))
            default:
                let raftError: Raft.Error = .incorrectRequest(request)
                result = .failure(E.init(raftError))
            }
            
            let newState: RequestState = .finished(Response(data: nil, statusCode: 0, error: error))
            requestDelegateProxy.invoke { $0.request(request, didChangeStateTo: newState) }
            didFailRequest(result: result)
        }
    }
    
    func finish() {
        if isExecuting {
            isExecuting = false
        }
        
        if !isFinished {
            isFinished = true
        }
    }
}

// MARK: - WorkerDelegate
extension Task: WorkerDelegate {
    public func worker(_ worker: Worker, didLoadResponse response: HTTPURLResponse, data: Data?) {
        let response = Response(data: data,
                                statusCode: response.statusCode,
                                error: nil,
                                httpURLResponse: response)
        let nextState: RequestState = .finished(response)

        guard
            requestDelegateProxy.reduce({ $0.request(request, shouldContinueFrom: nextState) }, initial: true)
        else {
            requestDelegateProxy.invoke { $0.request(request, didChangeStateTo: .cancelled) }
            finish()
            
            return
        }
        
        let result = mapper.map(response: response)
        requestDelegateProxy.invoke { $0.request(request, didChangeStateTo: nextState) }
        
        didFinishRequest(response: response, result: result)
    }
    
    public func worker(_ worker: Worker, didFailWithError error: Swift.Error) {
        
        let nextState: RequestState = .finished(Response(data: nil, statusCode: 0, error: error))
        
        guard
            requestDelegateProxy.reduce({ $0.request(request, shouldContinueFrom: nextState) }, initial: true)
        else {
            requestDelegateProxy.invoke { $0.request(request, didChangeStateTo: .cancelled) }
            finish()
            
            return
        }
        
        let response = Response(data: nil,
                                statusCode: 0,
                                error: error)
        let result = mapper.map(response: response)
        
        requestDelegateProxy.invoke { $0.request(request, didChangeStateTo: nextState) }
        
        didFinishRequest(response: response, result: result)
    }
    
    public func worker(_ worker: Worker, didChange progress: Progress) {
        requestDelegateProxy.invoke { $0.request(request, didChangeStateTo: .progress(progress)) }
        onProgress?(progress)
    }
    
    private func didFailRequest(result: Result<V, E>) {
        completion?(result)
        finish()
    }

    private func didFinishRequest(response: Response, result: Result<V, E>) {
        responseHandler?(response)
        completion?(completionMappingHandler?(response, result) ?? result)
        finish()
    }
    
    /// Map completion based on raw response object
    ///
    /// Example:
    ///
    ///     task.setCompletionMapping { response, result in
    ///        if response.statusCode == 409 {
    ///          return .failure(.loginAlreadyTaken)
    ///        }
    ///        return result
    ///     }
    ///
    /// Description:
    ///  - `Response`:  response object
    ///  - `Result`: result value based on mapping strategy
    ///
    /// - Parameter hander: will execute to capture response for result mapping
    ///
    /// - Returns: changed `Result<Value, Error>` value or initial one
    ///
    public func setCompletionMapping(handler: ((Response, Result<V, E>) -> Result<V, E>)?) {
        completionMappingHandler = handler
    }
    
    public func setResponse(hander: ResponseHandler?) {
        responseHandler = hander
    }
}
