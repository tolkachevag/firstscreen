//
//  Error.swift
//  Raft
//
//  Created by NAZARENKO Denis on 18/02/2019.
//

import Foundation

/// `RaftError` is the error type returned by Raft. It encompasses a few different types of errors, each with
/// their own associated reasons.
///
/// - network:                     Returned when a answer is related to network connection troubles
/// - explicitlyCancelled:         Returned when a `Request` is explicitly cancelled.
/// - missingUrl:                  Returned when a `URLConvertible` type fails to create a valid `URL`.
/// - parameterEncodingFailed:     Returned when a parameter encoding object throws an error during the encoding process.
/// - incorrectRequest:            Returned when a some request params during converting from Request to URLRequest throws error.
/// - responseSerializationFailed: Returned when a response serializer throws an error in the serialization process.
/// - underlyingError:             Returned when a response was got with some specific error.
/// - mappingError:                Returned when a mapper couldn't parse the fetched json.
/// - serverTrustEvaluationFailed: Returned when a `ServerTrustEvaluating` instance fails during the server trust evaluation process.
public enum Error: Swift.Error {
    case missingUrl
    case network(_ error: URLError)
    case parameterEncodingFailed(Swift.Error)
    case serverTrustEvaluationFailed
    case incorrectRequest(_ request: Request)
    case mappingError(_ response: Response)
    case underlyingError(_ error: Swift.Error)
}

extension Raft.Error: Equatable {
    public static func == (lhs: Raft.Error, rhs: Raft.Error) -> Bool {
        switch (lhs, rhs) {
        case let (.network(left), .network(right)): return left == right
        case (.missingUrl,.missingUrl): return true
        case (.parameterEncodingFailed,.parameterEncodingFailed) : return true
        case let (.incorrectRequest(left),.incorrectRequest(right)): return left == right
        case (.underlyingError(let left),.underlyingError(let right)):
            return left.localizedDescription == right.localizedDescription
        case let (.mappingError(left), .mappingError(right)): return left == right
        case (.serverTrustEvaluationFailed,.serverTrustEvaluationFailed): return true
        default:
        return false
        }
    }
}
