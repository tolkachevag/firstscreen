//
//  Raft.swift
//  Raft
//
//  Created by NAZARENKO Denis on 12/02/2019.
//

import Foundation.NSDictionary

// MARK: Raft.Service
public final class Service: NSObject {
        
    // MARK: - Public properties
    
    /// Cookie storage used to set cookies for all URLRequests
    /// It should be injected at Raft creating point.
    /// Use "HTTPCookieStorage.sharedCookieStorage(forGroupContainerIdentifier:)"
    public var cookieStorage: HTTPCookieStorage = HTTPCookieStorage.shared
    
    /// Validating hosts
    public var trustValidator: TrustValidator = .default
    
    // MARK: - Private properties
    
    /// Abstract worker used to perform network operation
    private let workerFactory: WorkerFactory
    /// RequestDelegateProxy
    var requestDelegateProxy = RequestDelegateProxy()
    /// Registered configurations
    private var markerConfigMap: [Marker: (configuration: Configurable, queue: OperationQueue)] = [:]
    
    // MARK: - Initializers
    
    public init(workerFactory: WorkerFactory = DefaultWorkerFactory()) {
        self.workerFactory = workerFactory
    }
    
    // MARK: - Public methods
    
    /// Register specific configuration and operation queue for the specified token
    ///
    /// - Parameters:
    ///   - configuration: Configuration used for generally setup request with specific params
    ///   - queue: OperationQueue where all tasks for registering marker should be executed
    ///   - marker: Microservice identifier
    public func register(configuration: Configurable, queue: OperationQueue, for marker: Marker) {
        markerConfigMap[marker] = (configuration: configuration, queue: queue)
    }
    
    /// Returns task configured for specific marker
    ///
    /// - Parameter marker: Specific marker responsible to any microservice
    /// - Returns: Just a task
    public func task<V, E>(marker: Marker) -> Task<V,E>? {
        guard let setup = markerConfigMap[marker] else { return nil }
        
        var request = Request(marker: marker)
        request.trust = trustValidator
        setup.configuration.apply(to: &request)
        
        return Task<V, E>(request: request,
                          workerFactory: workerFactory,
                          executingQueue: setup.queue,
                          requestDelegateProxy: requestDelegateProxy,
                          cookieStorage: cookieStorage)
    }
    
    /// Returns base url configured for specific marker
    ///
    /// - Parameter marker: Specific marker responsible to any microservice
    /// - Returns: Just a url
    public func baseUrl(for marker: Marker) -> URL? {
        guard let setup = markerConfigMap[marker] else { return nil }
        if let urlProvider = setup.configuration as? BaseURLProvider {
            return urlProvider.baseURL
        }
        return nil
    }
    
    public func add(delegate: RequestDelegate) {
        requestDelegateProxy.add(delegate: delegate)
    }
    
    public func remove(delegate: RequestDelegate) {
        requestDelegateProxy.remove(delegate: delegate)
    }
    
    public func configurationForMarker(_ marker: Marker) -> Configurable? {
        markerConfigMap[marker]?.configuration
    }
    
    public func queueForMarker(_ marker: Marker) -> OperationQueue? {
        markerConfigMap[marker]?.queue
    }
}
