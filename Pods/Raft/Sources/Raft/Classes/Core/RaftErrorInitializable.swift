//
//  RaftErrorInitializable.swift
//  Raft
//
//  Created by NAZARENKO Denis on 13/05/2019.
//

import Foundation

public protocol RaftErrorInitializable {
    init(_ raftError: Raft.Error)
}

extension Error: RaftErrorInitializable {
  public init(_ raftError: Error) {
    self = raftError
  }
}
