//
//  WorkerFactory.swift
//  Raft
//
//  Created by NAZARENKO Denis on 28/02/2019.
//

import Foundation

public protocol WorkerFactory {
    func make() -> Worker
}
