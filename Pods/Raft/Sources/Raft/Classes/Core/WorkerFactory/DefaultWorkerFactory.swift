//
//  DefaultWorkerFactory.swift
//  Raft
//
//  Created by NAZARENKO Denis on 28/02/2019.
//

import Foundation

public final class DefaultWorkerFactory: WorkerFactory {
    public func make() -> Worker {
        return URLSessionWorker()
    }
    
    public init() { }
}
