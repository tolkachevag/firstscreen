//
//  Response.swift
//  Raft
//
//  Created by NAZARENKO Denis on 12/02/2019.
//

import Foundation

public typealias StatusCode = Int

// MARK: - Response
public struct Response {
    
    // MARK: - Public properties
    
    public let data: Data?
    public let error: Swift.Error?
    public let statusCode: StatusCode
    public lazy var headers: HTTPHeaders = {
        guard let httpResponse = httpURLResponse else { return HTTPHeaders([]) }
        
        return HTTPHeaders(httpResponse
                            .allHeaderFields
                            .reduce([], { (result, next) -> [HTTPHeader] in
                                if let key = next.key as? String,
                                   let value = next.value as? String {
                                    return result + [HTTPHeader(name: key, value: value)]
                                } else {
                                    return result
                                }
                            })
        )
        
    }()
    
    public let httpURLResponse: HTTPURLResponse?
    
    public var isSuccessful: Bool {
        return error == nil && statusCode != 0
    }
    
    // MARK: - Initializers
    
    init(data: Data?,
         statusCode: StatusCode,
         error: Swift.Error?,
         httpURLResponse: HTTPURLResponse? = nil) {
        self.data = data
        self.statusCode = statusCode
        self.error = error
        self.httpURLResponse = httpURLResponse
    }
}

// MARK: Equatable
extension Response: Equatable {
    public static func == (lhs: Response, rhs: Response) -> Bool {
        return lhs.data == rhs.data
            && lhs.error?.localizedDescription == rhs.error?.localizedDescription
            && lhs.statusCode == rhs.statusCode
    }
}
