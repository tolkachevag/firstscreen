# Raft

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Getting Started

- extend class-container `Marker` with your own marker if needed or use existing (`sso`, `authentication`, etc)
- implement `Configurable` protocol or use existing `Configurable` implementations
- register `Configurable` & `OperationQueue` for __marker__.
- use `raft.task(for marker: Marker)` method to create typified task `Task<V, E: Error>`
- use `task.perform(completion:_)` method to execute task.
    - if you should be able to cancel task store the result of `task.perform(completion) -> Cancellable`
- added default `JSONMappingDescriptor` for mapping JSON
- use `sessionIdentifier` for creating URLSessionConfiguration in background mode

## Requirements
iOS 9.3+
## Installation

Raft is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'Raft'
```

## Author

NAZARENKO Denis, denis.nazarenko@raiffeisen.ru

## License

Raft is available under the MIT license. See the LICENSE file for more info.
