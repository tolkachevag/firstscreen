//
//  TestViewController.swift
//  FierstScreen
//
//  Created by TOLKACHEV Aleksey on 10.09.2021.
//

import UIKit

class TestViewController: UIViewController {
    
    let button = UIButton()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .green
        button.setTitle("Go to Red", for: .normal)
        button.backgroundColor = .white
        view.addSubview(button)
        button.setTitleColor(.black, for: .normal)
        button.frame = CGRect(x: 100, y: 100, width: 200, height: 52)
        button.addTarget(self, action: #selector(didTapButton), for: .touchUpInside)
    }
    
    @objc private func didTapButton() {
        print("didTapButton")
//        let rootVC = SecondViewController()
        
//        rootVC.title = "Welcome"
//        let navVC = UINavigationController(rootViewController: rootVC)
//        navVC.modalPresentationStyle = .fullScreen
//        present(navVC, animated: true)
    }
    

}


class SecondViewController: UIViewController {
    
    let button = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemRed
        title = "Welcome"
        
        button.setTitle("Go to Blue", for: .normal)
        button.backgroundColor = .white
        view.addSubview(button)
        button.setTitleColor(.black, for: .normal)
        button.frame = CGRect(x: 100, y: 100, width: 200, height: 52)
        button.addTarget(self, action: #selector(didTapButton), for: .touchUpInside)
    }
    
    @objc private func didTapButton() {
        print("didTapButton")
//        let vc = UIViewController()
//        let vc = ViewController()
//        vc.view.backgroundColor = .white
//        navigationController?.pushViewController(vc, animated: true)
    }
}
