//
//  LoginView.swift
//  FierstScreen
//
//  Created by TOLKACHEV Aleksey on 06.09.2021.
//

import UIKit

protocol LoginViewOutput: AnyObject {
    func handleSendForm(name: String, pass: String)
}


final class LoginView: UIView {
    
    let output: LoginViewOutput = ViewController()
    
    // MARK: - private var
    
    private let appearance = Appearance()
    
    private lazy var view: UIView = {
        var view = UIView()
        view.backgroundColor = UIColor(hex: "#F7F7F7")
        return view
    }()
    
    private lazy var icon = UIImageView(image: UIImage(named: "account"))
    
    private lazy var logotype: UIImageView = {
        var logotype = UIImageView(image: UIImage(named: "logotype"))
        return logotype
    }()
    
    private lazy var username: UITextField = {
        var username = UITextField()
        username.borderStyle = UITextField.BorderStyle.none
//        username.textColor = UIColor(hex: "#AAABAD")
        username.textColor = .gray
        username.tintColor = .gray
        username.placeholder = "Имя пользователя"
        username.font = username.font?.withSize(12)
        return username
    }()
    
    private lazy var password: UITextField = {
        var password = UITextField()
        password.borderStyle = UITextField.BorderStyle.none
        password.textColor = UIColor(hex: "#AAABAD")
        password.textColor = .gray
        password.placeholder = "Пароль"
        password.font = password.font?.withSize(12)
        return password
    }()
    
    private lazy var button: UIButton = {
        var button = UIButton(type: .custom)
        button.setTitle("Войти", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.backgroundColor = UIColor(hex: "#FEE600")
        button.layer.cornerRadius = 8
        button.addTarget(self, action: #selector(handleClick), for: .touchUpInside)
        return button
    }()
    
    private lazy var forgotPassword: UIButton = {
        var forgotPassword = UIButton(type: .custom)
        forgotPassword.setTitle("Забыли пароль?", for: .normal)
        forgotPassword.setTitleColor(UIColor(hex: "#AAABAD"), for: .normal)
        forgotPassword.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        return forgotPassword
    }()
    
    private lazy var bottomText: UITextView = {
        var bottomText = UITextView()
        bottomText.text = "2003-2021  АО Райфайзенбанк"
        bottomText.textAlignment = .center
        bottomText.font = bottomText.font?.withSize(11)
        bottomText.backgroundColor = .none
        bottomText.textColor = .gray
        return bottomText
    }()
    
    private lazy var tab: UIView = {
        var tab = UIView()
        tab.backgroundColor = UIColor(hex: "#ffffff")
        tab.layer.cornerRadius = 16
        return tab
    }()
    
    private lazy var tabTitle: UITextView = {
        var tabTitle = UITextView()
        tabTitle.text = "Открыть счет"
        tabTitle.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        tabTitle.backgroundColor = .none
        tabTitle.textColor = .black
        return tabTitle
    }()
    
    private lazy var tabDescription: UITextView = {
        var tabDescription = UITextView()
        tabDescription.text = "Получите реквизиты через 5 минут"
        tabDescription.backgroundColor = .none
        tabDescription.textColor = UIColor(hex: "#AAABAD")
        return tabDescription
    }()
    
    private lazy var lineUserName: UIView = {
        var lineUserName = UIView()
        lineUserName.frame.size = CGSize(width: username.frame.size.width, height: 2)
        lineUserName.frame.origin = CGPoint(x: 0, y: username.frame.maxY - lineUserName.frame.height + 5)
        lineUserName.backgroundColor = UIColor(hex: "#D5D5D6")
        lineUserName.autoresizingMask = [.flexibleWidth, .flexibleTopMargin]
        return lineUserName
    }()
    
    private lazy var linePassword: UIView = {
        var linePassword = UIView()
        linePassword.frame.size = CGSize(width: username.frame.size.width, height: 2)
        linePassword.frame.origin = CGPoint(x: 0, y: username.frame.maxY - linePassword.frame.height + 5)
        linePassword.backgroundColor = UIColor(hex: "#D5D5D6")
        linePassword.autoresizingMask = [.flexibleWidth, .flexibleTopMargin]
        return linePassword
    }()
    
    private lazy var stack: UIView = {
        var stack = UIView()
        return stack
    }()
    
    // MARK: - private func
    
    private func addSubviews() {
        [ view, logotype, button, forgotPassword, bottomText, tab, stack, icon, tabTitle, tabDescription, username, password ].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
        addSubview(view)
        [ logotype, bottomText, tab, stack ].forEach {
            view.addSubview($0)
        }
        [ icon, tabTitle, tabDescription ].forEach {
            tab.addSubview($0)
        }
        [ username, password, button, forgotPassword ].forEach {
            stack.addSubview($0)
        }
        username.addSubview(lineUserName)
        password.addSubview(linePassword)
    }

    private func setupConstraints() {
        NSLayoutConstraint.activate([
            view.topAnchor.constraint(equalTo: topAnchor),
            view.leadingAnchor.constraint(equalTo: leadingAnchor),
            view.trailingAnchor.constraint(equalTo: trailingAnchor),
            view.bottomAnchor.constraint(equalTo: bottomAnchor),
            
//            logotype.topAnchor.constraint(equalTo: view.topAnchor, constant: appearance.topOffset),
            logotype.topAnchor.constraint(equalTo: topAnchor, constant: appearance.viewInsets.top),
            logotype.leftAnchor.constraint(equalTo: view.leftAnchor, constant: appearance.sideOffset),
            
            stack.topAnchor.constraint(equalTo: logotype.bottomAnchor, constant: appearance.ziro),
            stack.bottomAnchor.constraint(equalTo: tab.topAnchor, constant: appearance.ziro),
            stack.leftAnchor.constraint(equalTo: view.leftAnchor, constant: appearance.sideOffset),
            stack.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -appearance.sideOffset),
            
            username.bottomAnchor.constraint(equalTo: password.topAnchor, constant: -appearance.verticalOffset),
            username.leftAnchor.constraint(equalTo: view.leftAnchor, constant: appearance.sideOffset),
            username.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -appearance.sideOffset),
            
            password.centerYAnchor.constraint(equalTo: stack.centerYAnchor),
            password.leftAnchor.constraint(equalTo: view.leftAnchor, constant: appearance.sideOffset),
            password.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -appearance.sideOffset),
            
            button.topAnchor.constraint(equalTo: password.topAnchor, constant: appearance.topOffset),
            button.leftAnchor.constraint(equalTo: view.leftAnchor, constant: appearance.sideOffset),
            button.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -appearance.sideOffset),
            button.heightAnchor.constraint(equalToConstant: appearance.heightAnchor),
            
            forgotPassword.topAnchor.constraint(equalTo: button.bottomAnchor, constant: appearance.ziro),
            forgotPassword.leftAnchor.constraint(equalTo: view.leftAnchor, constant: appearance.sideOffset),
            forgotPassword.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -appearance.sideOffset),
            forgotPassword.heightAnchor.constraint(equalToConstant: appearance.heightAnchor),
            
//            bottomText.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -appearance.tabSideOffset),
            bottomText.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -appearance.viewInsets.bottom),
            bottomText.leftAnchor.constraint(equalTo: view.leftAnchor, constant: appearance.sideOffset),
            bottomText.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -appearance.sideOffset),
            bottomText.heightAnchor.constraint(equalToConstant: appearance.bottomTextHeight),
            
            tab.heightAnchor.constraint(equalToConstant: appearance.tabHeight),
            tab.bottomAnchor.constraint(equalTo: bottomText.topAnchor, constant: -appearance.sideOffset),
            tab.leftAnchor.constraint(equalTo: view.leftAnchor, constant: appearance.sideOffset),
            tab.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -appearance.sideOffset),
            
            icon.widthAnchor.constraint(equalToConstant: appearance.verticalOffset),
            icon.heightAnchor.constraint(equalToConstant: appearance.verticalOffset),
            icon.topAnchor.constraint(equalTo: tab.topAnchor, constant: appearance.iconTopOffset),
            icon.leftAnchor.constraint(equalTo: tab.leftAnchor, constant: appearance.sideOffset),
            icon.widthAnchor.constraint(equalToConstant: appearance.verticalOffset),
            
            tabTitle.heightAnchor.constraint(equalToConstant: appearance.verticalOffset),
            tabTitle.leftAnchor.constraint(equalTo: icon.rightAnchor, constant: appearance.tabSideOffset),
            tabTitle.rightAnchor.constraint(equalTo: tab.rightAnchor, constant: appearance.sideOffset),
            tabTitle.topAnchor.constraint(equalTo: tab.topAnchor, constant: appearance.tabSideOffset),
            
            tabDescription.topAnchor.constraint(equalTo: tabTitle.bottomAnchor, constant: -appearance.sideOffset),
            tabDescription.leftAnchor.constraint(equalTo: icon.rightAnchor, constant: appearance.tabSideOffset),
            tabDescription.rightAnchor.constraint(equalTo: tab.rightAnchor, constant: -appearance.tabSideOffset),
            tabDescription.heightAnchor.constraint(equalToConstant: appearance.verticalOffset)
        ])
    }
    
    // MARK: - life circle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubviews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - actions

    @objc private func handleClick() {
        guard let userNameValue = username.text else { return }
        guard let passwordValue = password.text else { return }
        output.handleSendForm(name: userNameValue, pass: passwordValue)
    }
    
    private struct Appearance {
        let ziro: CGFloat = 0
        let tabSideOffset: CGFloat = 10
        let iconTopOffset:CGFloat = 16
        let sideOffset: CGFloat = 20
        let bottomTextHeight: CGFloat = 24
        let verticalOffset: CGFloat = 40
        let topOffset: CGFloat = 60
        let heightAnchor: CGFloat = 48
        let tabHeight: CGFloat = 80
        let viewInsets: UIEdgeInsets = UIEdgeInsets(top: 60, left: 16, bottom: 32, right: 16)
    }
    
}


// MARK: - extensions

extension UIColor {
    convenience init(hex: String) {
        var hexPrepare = hex.trimmingCharacters(in: .whitespacesAndNewlines)
        hexPrepare = hexPrepare.replacingOccurrences(of: "#", with: "")
        
        let length = hexPrepare.count
        
        var rgb: UInt64 = 0
        var r: CGFloat = 0.0
        var g: CGFloat = 0.0
        var b: CGFloat = 0.0
        var a: CGFloat = 1.0
        
        if Scanner(string: hexPrepare).scanHexInt64(&rgb) {
            if length == 6 {
                r = CGFloat((rgb & 0xFF0000) >> 16) / 255.0
                g = CGFloat((rgb & 0x00FF00) >> 8) / 255.0
                b = CGFloat(rgb & 0x0000FF) / 255.0
            } else if length == 8 {
                r = CGFloat((rgb & 0xFF000000) >> 24) / 255.0
                g = CGFloat((rgb & 0x00FF0000) >> 16) / 255.0
                b = CGFloat((rgb & 0x0000FF00) >> 8) / 255.0
                a = CGFloat(rgb & 0x000000FF) / 255.0
            } else {
                self.init(red: r, green: g, blue: b, alpha: a)
            }
        }
        
        self.init(red: r, green: g, blue: b, alpha: a)
    }

}
