//
//  Posts.swift
//  FierstScreen
//
//  Created by TOLKACHEV Aleksey on 12.09.2021.
//

import Foundation

// MARK: - Post
struct Post: Codable {
    let userID, id: Int?
    let title, body: String?

    enum CodingKeys: String, CodingKey {
        case userID = "userId"
        case id, title, body
    }
}

typealias Posts = [Post]
