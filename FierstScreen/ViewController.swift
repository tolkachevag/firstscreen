//
//  ViewController.swift
//  FierstScreen
//
//  Created by TOLKACHEV Aleksey on 06.09.2021.
//

import UIKit

final class ViewController: UIViewController {
    
    let testView = TestViewController()
    
    override func loadView() {
        super.loadView()
        view = LoginView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        ApiManager.shared.getUsers { users in
//            print(users.count)
//        }
    }

}

extension ViewController: LoginViewOutput {
    
    func handleSendForm(name: String, pass: String) {
        //        print("click")
//        let rootVC = SecondViewController()
        
//        rootVC.title = "Welcome"
        let navVC = UINavigationController(rootViewController: ViewController())
        navVC.modalPresentationStyle = .fullScreen
        present(navVC, animated: true)
        if name == "Alexey" && pass == "123" {
            print("success")
        } else {
            print("fail")
        }
    }
    
}
